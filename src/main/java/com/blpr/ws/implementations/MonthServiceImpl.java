package com.blpr.ws.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blpr.ws.beans.Month;
import com.blpr.ws.beans.Response;
import com.blpr.ws.repositories.MonthRepository;
import com.blpr.ws.services.MonthService;

@Service
public class MonthServiceImpl implements MonthService {

	
	
	@Autowired
	private MonthRepository monthRepository;
	
	@Override
	public Response getAllHoursByIsAndMonth(String isstr, Long month) {
		Month m = monthRepository.findByIsstrAndImes(isstr, month);
		Response res = new Response();
		if(m != null) { res.setResult(m); res.setTypeResponse("200 Succefully");}
		else { res.setResult(null); res.setTypeResponse("404 Not Found");}
		return res;
	}

	@Override
	public Response getAllUsersByMonth(Long month) {
		List<Month> m = monthRepository.findAllByImes(month);
		Response res = new Response();
		if(!m.isEmpty()) { res.setResult(m); res.setTypeResponse("200 Succefully");}
		else { res.setResult(null); res.setTypeResponse("404 Not Found");}
		return res;
	}

}
