package com.blpr.ws.controllers;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blpr.ws.beans.Response;
import com.blpr.ws.services.UpdateService;

@RestController
@RequestMapping("api/v1")
@CrossOrigin(origins = "http://localhost:4200")
public class UpdateController {

	@Autowired
	private UpdateService updateService;
	
	@GetMapping("/update")
    public Response updateInfo() throws ParseException, IOException {
        return updateService.updateAll();
    }
	
}
