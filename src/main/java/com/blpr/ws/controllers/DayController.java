package com.blpr.ws.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.blpr.ws.beans.Day;
import com.blpr.ws.beans.Response;
import com.blpr.ws.services.DayService;

@RestController
@RequestMapping("api/v1")
@CrossOrigin(origins = "http://localhost:4200")
public class DayController {
	
	@Autowired
	DayService dayService;

	@GetMapping(path =  "/days", produces = "application/json")
	public Response getAllUsers(){
		return dayService.getAll();
	}
	
	@PostMapping(path =  "/period", produces = "application/json")
	public Response getAllUsersByStrtDateAndEndDate(@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate  ){
		System.out.println("ENTRA AL getAllUsersByStrtDateAndEndDate" + startDate + " " + endDate);
		return dayService.getAllBySatartAndEndDate(startDate, endDate);
	}
	
	@PostMapping(path =  "/users", produces = "application/json")
	public Response getAllRecordsByStrtDateAndEndDateAndIs(@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate
			, @ModelAttribute("is") String is
			){
		System.out.println("ENTRA AL getAllUsersByStrtDateAndEndDate" + startDate + " " + endDate + " " + is);
		return dayService.getAllRecordsBySatartAndEndDateAndIs(is, startDate, endDate);
	}
	
	
    @RequestMapping("/")
    public String test() {
        return "Congratulations on creating your first controller";
    }


	
}
