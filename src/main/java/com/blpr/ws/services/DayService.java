package com.blpr.ws.services;

import java.util.List;


import com.blpr.ws.beans.Day;
import com.blpr.ws.beans.Response;

public interface DayService {

	Response getAll();
	Response getAllBySatartAndEndDate(String startDate, String endDate);
	Response getAllRecordsBySatartAndEndDateAndIs(String is, String startDate, String endDate);
}
