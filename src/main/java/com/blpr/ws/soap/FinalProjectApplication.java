package com.blpr.ws.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication(scanBasePackages = "com.blpr.ws")
@EnableTransactionManagement
@EnableJpaAuditing
@EntityScan("com.blpr.ws.beans")
@EnableJpaRepositories("com.blpr.ws.repositories")
public class FinalProjectApplication /*extends SpringBootServletInitializer*/ {
	
	/*@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(FinalProjectApplication.class);
	}*/

	public static void main(String[] args) {
		SpringApplication.run(FinalProjectApplication.class, args);
	}

}
