<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ page isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<%
	String contextPath = request.getContextPath();
%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<li><a href="<%= contextPath %>/api/v2/menu" >Back To Menu</a></li>
ALL RECORDS BY IS, START DAY AND END DAY
	<table border="2" width="70%" cellpadding="2">
         <tr>
            <th>Id</th>
            <th>Is</th>
            <th>Date</th>
            <th>Cin</th>
            <th>Cout</th>
            <th>Hours</th>
         </tr>
          <c:forEach items="${daysIsSdEd}" var="v">
              <tr>
              	  <td>${v.id}</td>
                  <td>${v.isstr}</td>
                  <td>${v.datestr}</td> 
                  <td>${v.cin}</td> 
                  <td>${v.cout}</td> 
                  <td>${v.hours}</td> 
               </tr>
          </c:forEach>
     </table> 
</body>
</html>