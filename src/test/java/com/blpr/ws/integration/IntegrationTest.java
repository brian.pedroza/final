package com.blpr.ws.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.blpr.ws.beans.Response;
import com.blpr.ws.soap.FinalProjectApplication;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes =  FinalProjectApplication.class )
public class IntegrationTest {

	@LocalServerPort
    private int port;
    
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    
    @Test
    public void testCreateStudent() throws Exception {
        //Setup
        String expectedIs = "CXMG";
        String expectedMonth = "5";
        String expectedStatus = "200 Succefully"; //Es el estado que arroja el Body si la peticion se hizo de forma correcta
        //Execute
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/api/v1/users/" + expectedIs + "/" + expectedMonth), HttpMethod.GET, entity, String.class);
        //Verify
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(response.getBody());
        System.out.println("RESPONSE"+ json.get("typeResponse"));
        assertEquals(expectedStatus, json.get("typeResponse"));
        System.out.println(response.getBody().toString());
    }    
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
    
    
    
    
/*    
    @Autowired
    private MockMvc mockMvc;
    @InjectMocks
    private DayController dayController;
    /*
    public void testCreateRetrieveWithMockMVC() throws Exception {
        this.mockMvc.perform(get("/students/1")).andDo(print()).andExpect(status().isOk());
    }
    */
    
    /*
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(dayController).build();
    }
    /*
    @Test
    public void testHelloWorld() throws Exception {
        when(.updateInfo()).thenReturn("Congratulations on updating the database");
        mockMvc.perform(get("api/v1/update"))
                .andExpect(status().isOk())
                .andExpect(content().string("Congratulations on updating the database"));
        verify(updateController).updateInfo();
    }*/
    /*
    @SuppressWarnings("deprecation")
    @Test
    public void testHelloWorldJson() throws Exception {
        mockMvc.perform(get("api/v1/days")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    */
}
